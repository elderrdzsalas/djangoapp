"""stores URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include, re_path
from stores.views import index, products, detail, results, StoreCreate, ProductCreate, StoreUpdate, ProductUpdate, StoreDelete, ProductDelete, contact, UserFormView, login_user, logout_user

app_name = "stores"
urlpatterns = [
    path('', index, name='index'),
    re_path(r'^(?P<store_id>[0-9]+)/$', detail, name='detail'),
    re_path(r'^st_add/$', StoreCreate.as_view(), name='store-add'),
    re_path(r'^st_addproduct/$', ProductCreate.as_view(), name='product-add'),
    re_path(r'^store/(?P<pk>[0-9]+)/$', StoreUpdate.as_view(), name='store-update'),
    re_path(r'^product/(?P<pk>[0-9]+)/$', ProductUpdate.as_view(), name='product-update'),
    re_path(r'^store/(?P<pk>[0-9]+)/delete/$', StoreDelete.as_view(), name='store-delete'),
    re_path(r'^product/(?P<pk>[0-9]+)/delete/$', ProductDelete.as_view(), name='product-delete'),
    re_path(r'^results/$',results,name='results'),
    re_path(r'^products/$',products,name='products'),
    re_path(r'^contact/$',contact,name='contact'),
    re_path(r'^register/$',UserFormView.as_view(),name='register'),
    re_path(r'^login_user/$',login_user,name='login_user'),
    re_path(r'^logout_user/$',logout_user,name='logout_user'),
]

