from django.contrib import admin
from stores.models import Store, Product

# Register your models here.

admin.site.register(Store)
admin.site.register(Product)

