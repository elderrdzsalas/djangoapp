from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django.contrib.auth.models import Permission, User

# Create your models here.

class Store(models.Model):
    id = models.CharField(max_length=9,primary_key=True)
    description =  models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    store_pic = models.FileField(null=True)
    user = models.ForeignKey(User, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return(self.id+" "+self.description+" "+self.address)

    def get_absolute_url(self):
        return reverse('stores:index')

class Product(models.Model):
    store = models.ForeignKey(Store,on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10,decimal_places=2)

    def __str__(self):
        return(self.id+" "+self.description+" "+self.price)

    def get_absolute_url(self):
        return reverse('stores:products')
