from django.shortcuts import render
from django.http import HttpResponse  
from stores.models import Store, Product
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.db.models import Q
from stores.forms import ContactForm
from django.shortcuts import redirect
import smtplib
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from .forms import UserForm
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

def index(request):
   if not request.user.is_authenticated:
    return render(request, 'login.html')
   else:
    all_stores = Store.objects.filter(user=request.user)
    context = {'all_stores':all_stores}
    return render(request,'index.html',context)

def products(request):
   if not request.user.is_authenticated:
    return render(request, 'login.html')
   else:
    all_products = Product.objects.all()
    context = {'all_products':all_products}
    return render(request,'products.html',context)

def detail(request,store_id):
  if not request.user.is_authenticated:
    return render(request, 'login.html')
  else:   
    my_store = Store.objects.get(pk=store_id)
    if my_store.user == request.user:
        store_product = Product.objects.filter(store=my_store)
        context = {'store_product':store_product,'my_store':my_store}
        return render(request,'detail.html',context)
    else:
        all_stores = Store.objects.filter(user=request.user)
        context = {'all_stores':all_stores}
        return render(request,'index.html',context)

class StoreCreate(LoginRequiredMixin, CreateView):
  login_url = '/stores/login_user/'
  model = Store
  fields = ['id','description','address','store_pic']
  
  def form_valid(self, form):
      obj = form.save(commit=False)
      obj.user = self.request.user
      obj.save()
      all_stores = Store.objects.filter(user=self.request.user)
      context = {'all_stores':all_stores}
      return render(self.request,'index.html',context)

class ProductCreate(LoginRequiredMixin, CreateView):
  login_url = '/stores/login_user/'
  model = Product
  fields = ['description','price','store']
  
  def form_valid(self, form):
      obj = form.save(commit=False)
      obj.user = self.request.user
      obj.save()
      all_stores = Store.objects.filter(user=self.request.user)
      context = {'all_stores':all_stores}
      return render(self.request,'index.html',context)

class StoreUpdate(LoginRequiredMixin, UpdateView):
  login_url = '/stores/login_user/'
  model = Store
  fields = ['id','description','address','store_pic']
  def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.user == request.user:
            return super(UpdateView, self).get(request, *args, **kwargs)
        else:
           all_stores = Store.objects.filter(user=request.user)
           context = {'all_stores':all_stores}
           return render(request,'index.html',context)

class ProductUpdate(LoginRequiredMixin, UpdateView):
  login_url = '/stores/login_user/'
  model = Product
  fields = ['description','price','store']
  def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.get_object():
            return super(UpdateView, self).get(request, *args, **kwargs)
        else:
           all_products = Product.objects.all()
           context = {'all_products':all_products}
           return render(request,'products.html',context)

class StoreDelete(LoginRequiredMixin, DeleteView):
    login_url = '/stores/login_user/'
    model = Store

    def delete(self, request, *args, **kwargs):
       self.object = self.get_object()
       if self.object.user == request.user:
          self.object.delete()
       all_stores = Store.objects.filter(user=request.user)
       context = {'all_stores':all_stores}
       return render(request,'index.html',context)

class ProductDelete(LoginRequiredMixin, DeleteView):
    login_url = '/stores/login_user/'
    model = Product

    def delete(self, request, *args, **kwargs):
       self.object = self.get_object()
       self.object.delete()
       all_products = Product.objects.all()
       context = {'all_products':all_products}
       return render(request,'products.html',context)       
 
def results(request):
   if not request.user.is_authenticated:
    return render(request, 'login.html')
   else:
    all_stores = Store.objects.all()
    if request.method == 'GET':  # If the form is submitted
        search_query = request.GET.get("q")
        all_stores = all_stores.filter((
            Q(id__icontains=search_query) |
            Q(description__icontains=search_query)) &
            Q(user=request.user)).distinct()
    context = {'all_stores':all_stores}
    return render(request,'index.html',context)

def contact(request):
   if not request.user.is_authenticated:
    return render(request, 'login.html')
   else:
    form_class = ContactForm
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
            , '')
            contact_email = request.POST.get(
                'contact_email'
            , '')
            form_content = request.POST.get('content', '')
            sender = 'scripts@itesm.edu'
            receivers = ['scripts@itesm.edu']
            message = """From: Stores application <scripts@itesm.edu>
To: <scripts@itesm.edu>
Subject: Stores contact message

Contact name: """ + contact_name + "\n" + "Contact email: " + contact_email + \
                         "\nMessage contents: \n" + form_content   

            smtpObj = smtplib.SMTP('localhost')
            smtpObj.sendmail(sender, receivers, message.encode("utf-8"))         
            return redirect('stores:index')

    context = {'form':form_class}
    return render(request,'contact.html',context)

class UserFormView(View):
    form_class = UserForm
    template_name = 'registration_form.html'
    # display a blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})
    # process form data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)

            #cleaned (normalized) data

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            #returns user objects if credentials are correct
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request,user)
                    return redirect('stores:index')
         # try again because it was not valid
        return render(request, self.template_name, {'form': form})

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                all_stores = Store.objects.filter(user=request.user)
                return render(request, 'index.html', {'all_stores': all_stores})
            else:
                return render(request, 'login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'login.html', {'error_message': 'Invalid login'})
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'login.html', context)
